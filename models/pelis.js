const Koa = require ("koa");
const KoaRouter = require("koa-router");
//const json = require ("koa-json");
const path = require ("path");
const render = require ("koa-ejs");
const { dirname } = require("path");
const app = new Koa();
const router = new KoaRouter();
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const pelisSchemas = new Schema({
Title: {type:String},
Year: {type:String},
Released: {type:String},
Genre: {type:String},
Director: {type:String},
Actors: {type:String},
Plot: {type:String},
Ratings: {type:String}

});

module.exports = Pelis = mongoose.model("Pelis", pelisSchemas);







