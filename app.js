const Koa = require ("koa");
const KoaRouter = require("koa-router");
//const json = require ("koa-json");
const path = require ("path");
const render = require ("koa-ejs");
const { dirname } = require("path");
const app = new Koa();
const router = new KoaRouter();
const mongoose = require('mongoose');

//app.use(json());
//ejemplo simple middleware
//app.use(async ctx => (ctx.body = { msg: 'hola mundo' }));

render(app, {
    root: path.join(__dirname, 'views'),
    layout: 'layout',
    viewExt: 'html',
    cache: false,
    debug:false
});
//index
router.get('/', async ctx => {
    await ctx.render('index');
});

router.get('/test', ctx => (ctx.body = "Hola Test"));

//Router Middleware
app.use(router.routes()).use(router.allowedMethods());


mongoose.connect(
    "mongodb://localhost/peliculas",
    { useNewUrlParser: true},
    (err, res) => {
        err && console.log("error conectando a la base de satos");
        app.listen(4000, () => {
            console.log("Servidor funcionando en http://localhost:4000/ ");
            console.log("Servidor funcionando en http://localhost:4000/test ");

        })
    
});


mongoose.connect('mongodb://localhost/peliculas').then(() => {
console.log("Connectado a la base de datos Alexis");
}).catch((err) => {

});

 //app.listen(3000, () => console.log("Servidor de Alexis Iniciado..."));


console.log("recuperando datos de API publica con APIKEY");
const fetch = require ("node-fetch");
fetch(" http://www.omdbapi.com/?i=tt3896198&apikey=c04a226e")
.then(promesaFetch => promesaFetch.json())
.then(contenido =>console.log(contenido));